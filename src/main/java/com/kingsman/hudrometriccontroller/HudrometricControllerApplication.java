package com.kingsman.hudrometriccontroller;

import com.google.api.core.ApiFuture;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.SubscriptionName;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import com.rabbitmq.client.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;

@SpringBootApplication
@EnableScheduling
public class HudrometricControllerApplication {

    public static void main(String[] args)  {
        SpringApplication.run(HudrometricControllerApplication.class, args);



}
@RestController
@RequestMapping("/watering")
class WateringController {
    private static final String REDIS_SERVER_HOST = "172.31.252.134";
    private static final int REDIS_SERVER_PORT = 6379;
    String broker = "tcp://172.31.252.134:1803";
    String topic = "watering-command-automatic/topic";
    String rabbitMQHost = "172.31.252.134";
    int rabbitMQPort = 1901;
    private Jedis jedis= new Jedis(REDIS_SERVER_HOST, REDIS_SERVER_PORT);

    private static final String projectId = "local-tempo-407113";
    private static final String credentialsPath = "src/main/resources/local-tempo-407113-f991c7c7667c.json";
    GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(credentialsPath))
            .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

    private static Publisher publisher = null;

    WateringController() throws IOException {
    }


    private static Connection getConnection(String host,int port) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        return factory.newConnection();
    }


    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        new Thread(this::checkAndPublishAutomaticChanges).start();
    }



    @GetMapping("/status")
    public String getWateringStatus() {

        // Retrieve the current watering status from Redis
        System.out.println(jedis.get("watering_status"));
        return jedis.get("watering_status");
    }

    @PostMapping("/toggle")
    public String toggleWatering() {

        // Toggle the watering status in Redis
        String currentStatus = jedis.get("watering_status");
        String newStatus = "on".equals(currentStatus) ? "off" : "on";
        jedis.set("watering_status", newStatus);

        // Publish the  command to RabbitMQ
        try {/*Connection connection = getConnection(rabbitMQHost,rabbitMQPort);
             Channel channel = connection.createChannel()) {
            channel.queueDeclare("commands", false, false, false, null);

            // Publish the message to RabbitMQ
            channel.basicPublish("", "commands", null, lastAutomaticStatus.getBytes());
            System.out.println("Sent to RabbitMQ: " + lastAutomaticStatus);*/



            ProjectTopicName topicName = ProjectTopicName.of(projectId, "commands");
            publisher = Publisher.newBuilder(topicName).setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

            publishToPubSub(newStatus,publisher);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return newStatus;
    }
    private volatile boolean isChangeFromApi = false;
    private String lastAutomaticStatus = "";
    @Async
    public void checkAndPublishAutomaticChanges()  {
        while (true) {
            try {
                // Fetch the current automatic watering status from Redis
                String currentStatus = jedis.get("watering_status-automatic");

                // Check for changes and publish updates if they are from the backend
                if (currentStatus != null && !currentStatus.equals(lastAutomaticStatus) && !isChangeFromApi) {
                    lastAutomaticStatus = currentStatus;

                    // Publish the automatic command to RabbitMQ
                    try {/*(Connection connection = getConnection(rabbitMQHost,rabbitMQPort);
                         Channel channel = connection.createChannel()) {
                        channel.queueDeclare("automatic-commands", false, false, false, null);

                        // Publish the message to RabbitMQ
                        channel.basicPublish("", "automatic-commands", null, lastAutomaticStatus.getBytes());
                        System.out.println("Sent to RabbitMQ: " + lastAutomaticStatus);
*/
                        ProjectTopicName topicName = ProjectTopicName.of(projectId, "automatic-commands");
                        publisher = Publisher.newBuilder(topicName).setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

                        publishToPubSub(lastAutomaticStatus,publisher);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    // Send the automatic command to Mosquitto
                    try {
                        MqttClient client = new MqttClient(broker, MqttClient.generateClientId());
                        client.connect();

                        MqttMessage mqttMessage = new MqttMessage();
                        mqttMessage.setPayload(currentStatus.getBytes());

                        client.publish(topic, mqttMessage);
                        System.out.println("message delivered to mosquitto "+mqttMessage);
                    } catch (MqttException e) {
                        e.printStackTrace(); // Handle the MQTT exception appropriately
                    }
                    jedis.set("watering_status", lastAutomaticStatus);
                }

                // Reset the flag for the next iteration
                isChangeFromApi = false;

                // Sleep for a specific duration before the next iteration
                Thread.sleep(5000); // Sleep for 5 seconds, adjust as needed
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                // Handle the interruption if needed
            }
        }
    }


}
    private static void publishToPubSub(String message, Publisher publisher) {
        ByteString data = ByteString.copyFromUtf8(message);
        PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

        try {
            // Once published, returns a server-assigned message ID (unique within the topic)
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();
            System.out.println("Published message ID: " + messageId);
            System.out.println("Sent to queue "+publisher.getTopicNameString()+" : " + message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}